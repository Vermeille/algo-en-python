#!/usr/bin/python2

import pygame, sys, random
from ship import ship
from pygame.locals import *

pygame.init()

#=================== INIT PYGAME =============================================
black = pygame.Color(0, 0, 0)
blue = pygame.Color(0, 0, 255)
white = pygame.Color(255, 255, 255)

window = pygame.display.set_mode((640, 480))
clock = pygame.time.Clock()
#===================== INIT GENETICS =========================================

myship = ship()
myship.speed_da = 0.0
myship.speed_db = 0.0
myship.speed_a = 0.0
myship.speed_b= 0.0
myship.rot_da = 0.0
myship.rot_db = 0.0
myship.rot_a = 0.0
myship.rot_b = 0.0

ships = []
scores = []
for i in range(1000):
    this_ship = ship()
    this_ship.mutate(myship, 10)
    ships.append(this_ship)
generation = 1
#========================= MAIN LOOP =========================================

def generation_full_mutation(ships):
    newships = []
    for caca in ships[:10]:
        for i in range(10):
            shp = ship()
            shp.mutate(caca, 20/generation)
            newships.append(shp)
    return newships

def generate_with_parents(ships):
    newships = []
    for i in range(100):
        shp = ship()
        shp.mutate_with_parents(random.choice(ships[:10]),
                random.choice(ships[:10]))
        newships.append(shp)
    return newships

while True:
    clock.tick(60)

    window.fill(black)

    for s in ships:
        pygame.draw.circle(window, blue, s.get_position(), 5, 0)
        pygame.draw.circle(window, white, s.get_pov(), 2, 0)

        s.update(clock.get_time()/1000.0)
        if not s.is_in_screen():
            ships.remove(s)
            scores.append(s)

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == MOUSEBUTTONUP:
            generation += 1
            ships.sort(key=ship.get_score, reverse=True)
            print("[")
            for caca in ships[:10]:
                print(caca)
            print("]")
            ships = generation_full_mutation(ships)
    pygame.display.update()
