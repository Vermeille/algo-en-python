import numpy, random
from math import *

class ship:
    def __init__(self):
        self.position = numpy.array([random.randint(0, 640), random.randint(0,
            480)])
        self.speed = 1.
        self.alpha = random.uniform(-1.0, 1.0)
        self.orientation = numpy.array([cos(self.alpha), sin(self.alpha)])
        self.d_reaction = 100.1
        self.d = 10.1
        self.score = 0

    def mutate(self, old, amp):
        self.speed_da = random.uniform(-amp, amp) + old.speed_da
        self.speed_db = random.uniform(-amp, amp) + old.speed_db
        self.speed_a = random.uniform(-amp, amp) + old.speed_a
        self.speed_b = random.uniform(-amp, amp) + old.speed_b
        self.rot_da = random.uniform(-amp, amp)*0.01 + old.rot_da
        self.rot_db = random.uniform(-amp, amp)*0.01 + old.rot_db
        self.rot_a = random.uniform(-amp, amp)*0.01 + old.rot_a
        self.rot_b = random.uniform(-amp, amp)*0.01 + old.rot_b
        self.d_reaction = random.uniform(-amp, amp)*4. + old.d_reaction

    def mutate_with_parents(self, p1, p2):
        rand = random.random()
        if rand < 0.1:
            self.speed_da = random.uniform(-1.0, 1.0)
        elif rand < 0.5:
            self.speed_da = p1.speed_da
        else:
            self.speed_da = p2.speed_da

        rand = random.random()
        if rand < 0.1:
            self.speed_db = random.uniform(-1.0, 1.0)
        elif rand < 0.5:
            self.speed_db = p1.speed_db
        else:
            self.speed_db = p2.speed_db

        rand = random.random()
        if rand < 0.1:
            self.speed_a = random.uniform(-1.0, 1.0)
        elif rand < 0.5:
            self.speed_a = p1.speed_a
        else:
            self.speed_a = p2.speed_a

        rand = random.random()
        if rand < 0.1:
            self.speed_b = random.uniform(-1.0, 1.0)
        elif rand < 0.5:
            self.speed_b = p1.speed_b
        else:
            self.speed_b = p2.speed_b

        rand = random.random()
        if rand < 0.1:
            self.rot_da = random.uniform(-1.0, 1.0)
        elif rand < 0.5:
            self.rot_da = p1.rot_da
        else:
            self.rot_da = p2.rot_da

        rand = random.random()
        if rand < 0.1:
            self.rot_db = random.uniform(-1.0, 1.0)
        elif rand < 0.5:
            self.rot_db = p1.rot_db
        else:
            self.rot_db = p2.rot_db

        rand = random.random()
        if rand < 0.1:
            self.rot_a = random.uniform(-1.0, 1.0)
        elif rand < 0.5:
            self.rot_a = p1.rot_a
        else:
            self.rot_a = p2.rot_a

        rand = random.random()
        if rand < 0.1:
            self.rot_b = random.uniform(-1.0, 1.0)
        elif rand < 0.5:
            self.rot_b = p1.rot_b
        else:
            self.rot_b = p2.rot_b

        rand = random.random()
        if rand < 0.1:
            self.d_reaction = random.uniform(-1.0, 1.0)
        elif rand < 0.5:
            self.d_reaction = p1.d_reaction
        else:
            self.d_reaction = p2.d_reaction


    def update(self, time):
        self.d = self.get_d()
        if (self.d_reaction > self.d):
            self.speed += (self.speed_da * self.d + self.speed_db) * time
            self.alpha = (self.rot_da * self.d + self.rot_db) * time
        else:
            self.speed += (self.speed_a * self.d + self.speed_b) * time
            self.alpha = (self.rot_a   * self.d + self.rot_b) * time
        x = self.orientation[0] * cos(self.alpha) \
                - self.orientation[1] * sin(self.alpha)
        y = self.orientation[0] * sin(self.alpha) \
                + self.orientation[1] * cos(self.alpha)
        self.orientation[0] = x
        self.orientation[1] = y
        self.speed *= 0.885
        self.position = self.position + self.orientation * self.speed * time
        if self.speed >= 10:
            self.score += self.speed

    def get_d(self):
        if self.orientation[0] != 0.:
            t = - self.position[0] / self.orientation[0]
            t2 = self.orientation[1] * t + self.position[1]
            if 0 <= t2 <= 480:
                return self.distance(0, t2)
            t = (640 - self.position[0]) / self.orientation[0]
            t2 = self.orientation[1] * t + self.position[1]
            if 0 <= t2 <= 480:
                return self.distance(640, t2)

        if self.orientation[1] != 0.:
            t = (480 - self.position[1]) / self.orientation[1]
            t2 = self.orientation[0] * t + self.position[0]
            if t >= 0 and 0 <= t2 <= 640:
                return self.distance(t2, 480)
            t = - self.position[1] / self.orientation[1]
            t2 = self.orientation[0] * t + self.position[0]
            if 0 <= t2 <= 640:
                return self.distance(t2, 0)

    def get_position(self):
        return (int(self.position[0]), int(self.position[1]))

    def get_pov(self):
        return (int(5*self.orientation[0] + self.position[0]), \
                int(5*self.orientation[1] + self.position[1]))

    def distance(self, x, y):
        return sqrt(pow(self.position[0] - x, 2)+ \
            pow(self.position[1] - y, 2))

    def is_in_screen(self):
        return 1 < self.position[0] < 640 and 1 < self.position[1] < 480

    def get_score(self):
        return self.score

    def __str__(self):
        return str(self.score)

