#!/usr/bin/python2.7

import random
import os

def generateLine(len):
    line = []
    for i in range(len):
        line.append(random.choice([True, False, False, False]))

    return line

def generateGrid(x, y):
    grid = []
    for i in range(y):
        grid.append(generateLine(x))

    return grid

def printGrid(grid):
    for line in grid:
        linestr = ""
        for item in line:
            linestr += "X" if item else " "
        print(linestr)

def livingCellsAround(grid, x, y):
    living = 0
    for i in range(8):
        if i != 4:
            living += 1 if grid[(x + i/3) % len(grid)][(y + i%3) % len(grid[x])] else 0
    return living

def nextCellState(cell, living):
    if living < 2:
        return False
    elif living == 2:
        return cell
    elif living == 3:
        return True
    else:
        return False

def nextGridState(grid):
    newGrid = []
    for x, line in enumerate(grid):
        newLine = []
        for y, item in enumerate(line):
            newLine.append(\
                    nextCellState(grid[x][y], livingCellsAround(grid,x, y)))
        newGrid.append(newLine)
    return newGrid

grid = generateGrid(100, 50)

while True:
    os.system("clear")
    printGrid(grid)
    grid = nextGridState(grid)
    #os.system("sleep 0.01")
